import hashlib
import os
import subprocess

import bs4
import requests
import werkzeug.exceptions
from flask import Flask, render_template, abort, request
from flask_gzip import Gzip


app = Flask(__name__)
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True
gzip = Gzip(app)  # gzip response

# Application metadata stuff (version etc)
app.config["GIT_DESCRIBE"] = subprocess.check_output(["git", "describe", "--long"]).strip().decode()


BASE_URL = "http://cloud.tfl.gov.uk/TrackerNet"
ATTRS = ["Location", "Destination", "TripNo", "TimeTo", "DepartTime", "DepartInterval", "TrackCode"]


def get_api_keys():
    return {
        "app_id": os.environ.get("TRACKERNET_APP_ID"),
        "app_key": os.environ.get("TRACKERNET_APP_KEY")
    }


def sha256(m):
    return hashlib.sha256(str.encode(str(m))).hexdigest()


# Do not cache any pages.
@app.after_request
def add_header(response):
    response.cache_control.max_age = 0
    response.cache_control.no_cache = True
    return response


@app.route("/")
def main():
    return render_template("home.html")


@app.route("/prediction")
def prediction():
    line = request.args.get("line")
    station = request.args.get("station")

    if not (line and station):
        abort(400)

    url = f"{BASE_URL}/PredictionDetailed/{line}/{station}"

    keys = get_api_keys()
    params = dict()
    if keys["app_id"] and keys["app_key"]:
        params.update(keys)

    resp = requests.get(url, params=params)
    if not resp.ok:
        abort(502, resp)

    result = bs4.BeautifulSoup(resp.content, "xml")

    # For errors that masquerade as 200s but contain some dodgy
    # error message within that cannot be parsed as XML.
    # (Seriously, who thought this was a good idea?)
    if not result.ROOT:
        abort(502, resp)

    return render_template("prediction.html", **{
        "result": result.ROOT,
        "attrs": ATTRS
    })


@app.route("/about")
def about():
    return render_template("about.html")


@app.route("/keycheck")
def key_check():
    keys = get_api_keys()

    return render_template("key_check.html", **{
        "app_keys_set": (keys["app_id"] and keys["app_key"]),
        "sha256_app_id": sha256(keys["app_id"]),
        "sha256_app_key": sha256(keys["app_key"])
    })


@app.errorhandler(werkzeug.exceptions.BadRequest)
def handle_bad_request(_):
    return render_template("400.html"), 400


@app.errorhandler(werkzeug.exceptions.BadGateway)
def handle_bad_gateway(e):
    return render_template("502.html", e=e), 502


if __name__ == '__main__':
    app.run()
