beautifulsoup4 >=4.6, <5
flask >=1.0, <2
flask-gzip >=0.2, <0.3
gunicorn >=20.0, <21
lxml >=4.2, <5
requests >=2.21, <3
werkzeug >=0.16, <0.17
